
<?php

// buat trait dulu
trait Hewan {
    public $nama;
    public $darah = 50;
    public $jumlahKaki;
    public $keahlian;
    //didalem siini buat method namanya atraksi
    public function atraksi() {
        echo "{$this->nama} sedang {$this->keahlian}";
    }
}
// buat fight ke class abstrac
abstract class Fight{
    use Hewan; // supaya seluruh properti Hewan dan properti dari class Fight bisa di gunakan di class Elang
    public $attackPower;
    public $defencePower;
     //didalem siini buat method namanya serang
     public function serang($hewan) //$hewan ini memiliki seluruh properti hewan dan fight karna sudah masuk ke trait dan abstrac di atas 
    {
        echo "{$this->nama} sedang menyerang {$hewan->nama}";
        //method diserang() otomatis dipanggil jika method serang() dipanggil.
        echo "<br>";
        $hewan->diserang($this);
    }
    public function diserang($hewan){
        echo "{$this->nama} sedang diserang {$hewan->nama}";
       //kemudian ketika diserang akan berkurang darah nya
        $this->darah = $this->darah - ($hewan->attackPower / $this->defencePower);
    }

    protected function getInfo() {

      echo "Nama : {$this->nama}";
      echo "<br>";
      echo "Jumlah Kaki : {$this->jumlahKaki}";
      echo "<br>";
      echo "Keahlian : {$this->keahlian}";
      echo "<br>";
      echo "Darah : {$this->darah}";
      echo "<br>";
      echo "attackPower : {$this->attackPower}";
      echo "<br>";
      echo "defencePower : {$this->defencePower}";
      echo "<br>";

    }






    abstract public function getInfoHewan();
}

//class biasa elang
class Elang extends Fight{  // biar bisa dipake disini ini namanya di instansiasi dibawah ini

    public function __construct($nama) {
        $this->nama = $nama;
        $this->jumlahKaki = 2;
        $this->keahlian = "terbang tinggi";
        $this->attackPower = 10;
        $this->defencePower =5;
    }



    public function getInfoHewan() {
        echo "Jenis Hewan : Elang";
        echo "<br>";

        $this->getInfo();
   

    }

} 
//class biasa harimau
class Harimau extends Fight{ 
    public function __construct($nama){
        $this->nama = $nama;
        $this->jumlahKaki = 4;
        $this->keahlian = "lari cepat";
        $this->attackPower = 7;
        $this->defencePower =8;

    }

    //protected function getInfo()
    public function getInfoHewan() {
        echo "Jenis Hewan : Harimau";
        echo "<br>";
      
        $this->getInfo();

    }
}

class Spasi{
  public static function tampilkan(){
    echo "<br>";
    echo "=================";
    echo "<br>";
  }

}

$harimau = new Harimau("Harimau Sumatera");
$harimau->getInfoHewan();
Spasi::tampilkan();
$elang = new Elang("Elang Jawa");
$elang->getInfoHewan();
Spasi::tampilkan();
$harimau->serang($elang);
Spasi::tampilkan();
$elang->getInfoHewan();
Spasi::tampilkan();

$elang->serang($harimau);
Spasi::tampilkan();
$harimau->getInfoHewan();




?>
