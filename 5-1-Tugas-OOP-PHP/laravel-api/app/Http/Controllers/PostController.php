<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Post;
use Illuminate\Support\Facades\Validator;

class PostController extends Controller
{ //ini middleware
    //supaya ketika create sesuatu di tabel post.. akan diarahkan ke login dulu

    public function __construct()
    {
        return $this->middleware('auth:api')->only(['store', 'update', 'delete']);
    }
    //sampe sini, karna di atas yang mau di proteksei middleware cuma method store, update, delete maka tambahin only


    public function index()
    {
        $posts = Post::latest()->get();
        return response()->json([
            'success' => true,
            'message' => 'Data Post Berhasil di Tampilkan',
            'data'    => $posts
        ]);
    }

    public function store(Request $request)
    {
        $request_all = $request->all();
        $validator = Validator::make($request_all, [
            'title' => 'required',
            'description' => 'required'

        ]);
        if ($validator->fails()) {
            return response()->json($validator->errors(), 400);
        }


        $post = Post::create([
            'title' => $request->title,
            'description' => $request->description,
        ]);

        if ($post) {
            return response()->json([
                'success' => true,
                'message' => 'Data Post Berhasil dibuat',
                'data'    => $post
            ], 200);
        }
        return response()->json([
            'success' => false,
            'message' => 'Data Post Tidak Berhasil dibuat'
        ], 409);
    } //ini tutup function punya store
    public function show(Request $request) // boleh juga di tulis $id
    {
        $post = Post::find($request->id); // ntar disini panggil aj $id // pakai find biar muncul pesan error nya kalo ga ketemu id yang mau di show

        if ($post) {
            return response()->json([
                'success' => true,
                'message' => 'Data Post Berhasil di tampilkan',
                'data'    => $post
            ], 200); //200 ini boleh ditulis boleh nggak
        }
        return response()->json([
            'success' => false,
            'message' => 'Data Post dengan id : ' . $request->id . ' tidak di temukan'
        ], 404);
    }

    public function update(Request $request, $id)
    {
        $request_all = $request->all();
        $validator = Validator::make($request_all, [
            'title' => 'required',
            'description' => 'required'

        ]);
        if ($validator->fails()) {
            return response()->json($validator->errors(), 400);
        }


        $post = Post::find($id); //boleh find boleh findOrFile
        //disini buat event listener materi
        //kode disini gaes=>

        //auth login




        if ($post) { //bacanya jka $posnya ada maka

            $user = auth()->user();

            if ($post->user_id != $user->id) {
                return response()->json([
                    'success' => false,
                    'message' => 'Data post bukan milik anda',
                ], 403);
            }

            $post->update([
                'title' => $request->title,
                'description' => $request->description,
            ]);

            return response()->json([
                'success' => true,
                'message' => 'Data Post dengan judul : ' . $post->title . ' Berhasil di update',
                'data'    => $post
            ], 200);
        }

        return response()->json([
            'success' => false,
            'message' => 'Data Post dengan id : ' . $id . ' tidak di temukan'
        ], 404);
    }
    public function destroy($id)
    {
        $post = Post::find($id); //boleh find boleh findOrFile


        if ($post) { //bacanya jka $posnya ada isinya maka

            $user = auth()->user();

            if ($post->user_id != $user->id) {
                return response()->json([
                    'success' => false,
                    'message' => 'Data post bukan milik user login',
                ], 403);
            }

            $post = $post->delete();

            return response()->json([
                'success' => true,
                'message' => 'Data Post Berhasil di delete',
                'data'    => $post
            ], 200);
        }
        return response()->json([
            'success' => false,
            'message' => 'Data Post dengan id : ' . $id . ' tidak di temukan'
        ], 404);
    }
}
