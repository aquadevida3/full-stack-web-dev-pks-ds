<?php

namespace App\Http\Controllers;

use App\Comments;
// use App\Mail\PostAuthorMail;
use Illuminate\Http\Request;
// use App\Mail\CommentAuthorMail;
use App\Events\CommentStoredEvent;
// use Illuminate\Support\Facades\Mail;
use Illuminate\Support\Facades\Validator;

class CommentsController extends Controller
{
    //ini middleware
    //supaya ketika create sesuatu di tabel post.. akan diarahkan ke login dulu

    public function __construct()
    {
        return $this->middleware('auth:api')->except(['index', 'delete']);
    }
    //sampe sini, karna di atas yang mau di proteksei middleware seluruh method store, update, delete maka tambahin except (kecuali)
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    //public function index($post_id) //karna comment ini bergantung dari tabel post maka kita tambahkan variable $post_id
    public function index(Request $request)
    {
        $post_id = $request->post_id;

        $comments = Comments::where('post_id', $post_id)->latest()->get();
        return response()->json([
            'success' => true,
            'message' => 'Comments Berhasil di Tampilkan',
            'data'    => $comments
        ]);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $request_all = $request->all();
        $validator = Validator::make($request_all, [
            'content' => 'required',
            'post_id' => 'required'

        ]);
        if ($validator->fails()) {
            return response()->json($validator->errors(), 400);
        }


        $comments = Comments::create([
            'content' => $request->content,
            'post_id' => $request->post_id,
        ]);

        //memanggil event CommentStoredEvent
        event(new CommentStoredEvent($comments));


        //mengirim email ke pembuat post ngasih tau bahwa postingannya di comen 
        //:: (static method namanya)
        //dd($comments->post->user->email);

        //karna ga pakai ini lagi klo pakai even listener kita buat commentar dulu //
        //ini dikirim kepada yang memiliki post
        // Mail::to($comments->post->user->email)->send(new PostAuthorMail($comments));


        //ini dikirim kepada yang memiliki Comments
        // Mail::to($comments->user->email)->send(new CommentAuthorMail($comments));


        if ($comments) {
            return response()->json([
                'success' => true,
                'message' => 'Comments Berhasil dibuat',
                'data'    => $comments
            ], 200);
        }
        return response()->json([
            'success' => false,
            'message' => 'Data Comments Tidak Berhasil dibuat'
        ], 409);
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $comments = Comments::find($id); // ntar disini panggil aj $id // pakai find biar muncul pesan error nya kalo ga ketemu id yang mau di show

        if ($comments) {
            return response()->json([
                'success' => true,
                'message' => 'Comments Berhasil di tampilkan',
                'data'    => $comments
            ], 200); //200 ini boleh ditulis boleh nggak
        }
        return response()->json([
            'success' => false,
            'message' => 'Data Comments dengan id : ' . $id . ' tidak di temukan'
        ], 404);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $request_all = $request->all();
        $validator = Validator::make($request_all, [
            'content' => 'required' //kita tidak butuh post_id lagi karna update comment ga mungkin di id yang berbeda
        ]);

        if ($validator->fails()) {
            return response()->json($validator->errors(), 400);
        }

        $comments = Comments::find($id); //boleh find boleh findOrFile

        if ($comments) { //bacanya jka $posnya ada maka

            $user = auth()->user();

            if ($comments->user_id != $user->id) {
                return response()->json([
                    'success' => false,
                    'message' => 'Data comments bukan milik user login',
                ], 403);
            }

            $comments->update([
                'content' => $request->content
            ]);

            return response()->json([
                'success' => true,
                'message' => 'Comment Berhasil di update',
                'data'    => $comments
            ], 200);
        }
        return response()->json([
            'success' => false,
            'message' => 'Data comments dengan id : ' . $id . ' tidak di temukan'
        ], 404);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $comments = Comments::find($id); //boleh find boleh findOrFile


        if ($comments) { //bacanya jka $posnya ada isinya maka

            $user = auth()->user();

            if ($comments->user_id != $user->id) {
                return response()->json([
                    'success' => false,
                    'message' => 'Data comments bukan milik user login',
                ], 403);
            }


            $comments = $comments->delete();

            return response()->json([
                'success' => true,
                'message' => 'Data Comment Berhasil di delete',
                'data'    => $comments
            ], 200);
        }
        return response()->json([
            'success' => false,
            'message' => 'Comment dengan id : ' . $id . ' tidak di temukan'
        ], 404);
    }
}
