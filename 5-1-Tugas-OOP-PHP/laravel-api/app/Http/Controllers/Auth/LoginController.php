<?php

namespace App\Http\Controllers\Auth;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Validator;

class LoginController extends Controller
{
    /**
     * Handle the incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function __invoke(Request $request)
    {
        //dd('masuk ke login');
        $request_all = $request->all();
        $validator = Validator::make($request_all, [
            'email' => 'required',
            'password' => 'required'

        ]);
        if ($validator->fails()) {
            return response()->json($validator->errors(), 400);
        }

        $credentials = request(['email', 'password']);

        if (!$token = auth()->attempt($credentials)) { //fungsi method attemt itu mengecek mengambil model user apa kah sama, $credential apakah ditable email dan password benar.. jika benar maka rilis token
            return response()->json([
                'success' => 'false',
                'message' => 'Email atau Password tidak ditemukan'
            ], 401);
        }

        return response()->json([
            'success' => 'true',
            'message' => 'User berhasil login',
            'data'    => [
                'user' => auth()->user(),
                'token' => $token
            ]
        ], 200);
    }
}
