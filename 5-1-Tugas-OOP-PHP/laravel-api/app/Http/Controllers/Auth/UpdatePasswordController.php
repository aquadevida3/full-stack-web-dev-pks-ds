<?php

namespace App\Http\Controllers\Auth;

use App\User;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Validator;

class UpdatePasswordController extends Controller
{
    /**
     * Handle the incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function __invoke(Request $request)
    {
        // dd('masuk');
        $request_all = $request->all();
        $validator = Validator::make($request_all, [
            'email' => 'required',
            'password' => 'required|confirmed|min:6' //bisa liat di documentasi laravel nya untuk buat combinasi angka dsb

        ]);
        if ($validator->fails()) {
            return response()->json($validator->errors(), 400);
        }

        $user = User::where('email', $request->email)->first();

        if (!$user) {
            return response()->json([
                'success' => false,
                'message' => 'email Tidak ditemukan'
            ], 400);
        } //jika $user ada maka

        $user->update([
            'password' => Hash::make($request->password) //$request->password  nah jangan langsung request->password , gunakan hashing di documentasi laravel ada baca aja
        ]);

        return response()->json([
            'success' => true,
            'message' => 'Password berhasil di ubah',
            'data' => $user
        ], 200);
    }
}
