<?php

namespace App\Http\Controllers\Auth;


use App\User;
use App\OtpCode;
use Carbon\Carbon;
use Illuminate\Http\Request;
use App\Events\OtpCodeStoredEvent;
//use App\Users as AppUsers;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Validator;

class RegisterController extends Controller
{
    /**
     * Handle the incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function __invoke(Request $request)
    {
        //dd('masuk'); ngecek data masuk apa nggak biasakan gini dulu
        $request_all = $request->all();
        $validator = Validator::make($request_all, [
            'name' => 'required',
            'email' => 'required|unique:users,email|email', //supaya email berformat email tidak string biasa email|email
            //maksudnya: email harus uniq ke tabel users -> di kolom email , email harus pakai format email
            'username' => 'required|unique:users,username'

        ]);
        if ($validator->fails()) {
            return response()->json($validator->errors(), 400);
        }

        $user = User::create($request_all);

        //generate otp code 
        do {
            $random = mt_rand(100000, 999999);

            $check = OtpCode::where('otp', $random)->first();
        } while ($check);

        $now = Carbon::now();

        $otp_code = OtpCode::create([
            'otp' => $random,
            'valid_until' => $now->addMinutes(5),
            'user_id' => $user->id
        ]);

        //memanggil event OtpCodeStoredEvent
        event(new OtpCodeStoredEvent($otp_code, true));

        return response()->json([
            'success' => true,
            'message' => 'Data User Berhasil dibuat',
            'data' => [
                'user' => $user,
                'otp_code' => $otp_code
            ]
        ]);
    }
}
