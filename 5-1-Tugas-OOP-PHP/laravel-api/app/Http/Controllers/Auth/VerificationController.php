<?php

namespace App\Http\Controllers\Auth;

use App\User;
use App\OtpCode;
use Carbon\Carbon;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Validator;

class VerificationController extends Controller
{
    /**
     * Handle the incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function __invoke(Request $request)
    {
        // dd('masuk');
        $request_all = $request->all();
        $validator = Validator::make($request_all, [
            'otp' => 'required'
        ]);
        if ($validator->fails()) {
            return response()->json($validator->errors(), 400);
        }

        $otp_code = OtpCode::where('otp', $request->otp)->first();

        if (!$otp_code) { //jika otp_code ga ada maka 
            return response()->json([
                'success' => false,
                'message' => 'OTP Code Tidak ditemukan'
            ], 400);
        } //tetapi jika ditemukan maka

        $now = Carbon::now();

        if ($now > $otp_code->valid_until) { //jika sekarang lebih besar dari otp code valid until nnya maka codenya sudah tidak berlaku karna cuma 5 menit
            return response()->json([
                'success' => false,
                'message' => 'OTP Code tidak Berlaku lagi '
            ], 400);
        }
        //jika otp code ada didatabase dan masih berlaku valid untilnya maka

        $user = User::find($otp_code->user_id);
        $user->update([
            'email_verified_at' => $now
        ]);
        $otp_code->delete();

        return response()->json([
            'success' => true,
            'message' => 'user berhasil diverifikasi ',
            'data'    => $user
        ], 200);
    }
}
