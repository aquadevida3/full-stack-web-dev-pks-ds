<?php

namespace App\Http\Controllers;

use App\Roles;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;

class RoleController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $role = Roles::latest()->get();
        return response()->json([
            'success' => true,
            'message' => 'Data Role Berhasil di Tampilkan',
            'data'    => $role
        ]);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $request_all = $request->all();
        $validator = Validator::make($request_all, [
            'name' => 'required'
        ]);
        if ($validator->fails()) {
            return response()->json($validator->errors(), 400);
        }


        $role = Roles::create([
            'name' => $request->name
        ]);

        if ($role) {
            return response()->json([
                'success' => true,
                'message' => 'Data role Berhasil dibuat',
                'data'    => $role
            ], 200);
        }
        return response()->json([
            'success' => false,
            'message' => 'Data Role Tidak Berhasil dibuat'
        ], 409);
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $role = Roles::find($id); // ntar disini panggil aj $id // pakai find biar muncul pesan error nya kalo ga ketemu id yang mau di show

        if ($role) {
            return response()->json([
                'success' => true,
                'message' => 'Data Role Berhasil di tampilkan',
                'data'    => $role
            ], 200); //200 ini boleh ditulis boleh nggak
        }
        return response()->json([
            'success' => false,
            'message' => 'Data Role dengan id : ' . $id . ' tidak di temukan'
        ], 404);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $request_all = $request->all();
        $validator = Validator::make($request_all, [
            'name' => 'required'
        ]);
        if ($validator->fails()) {
            return response()->json($validator->errors(), 400);
        }

        $role = Roles::find($id); //boleh find boleh findOrFile

        if ($role) { //bacanya jka $posnya ada maka
            $role->update([
                'name' => $request->name
            ]);
            return response()->json([
                'success' => true,
                'message' => 'Data Role dengan Name : ' . $role->name . ' Berhasil di update',
                'data'    => $role
            ], 200);
        }
        return response()->json([
            'success' => false,
            'message' => 'Data Role dengan id : ' . $id . ' tidak di temukan'
        ], 404);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $role = Roles::find($id); //boleh find boleh findOrFile


        if ($role) { //bacanya jka $posnya ada isinya maka
            $role = $role->delete();

            return response()->json([
                'success' => true,
                'message' => 'Data Role Berhasil di delete',
                'data'    => $role
            ], 200);
        }
        return response()->json([
            'success' => false,
            'message' => 'Data Role dengan id : ' . $id . ' tidak di temukan'
        ], 404);
    }
}
