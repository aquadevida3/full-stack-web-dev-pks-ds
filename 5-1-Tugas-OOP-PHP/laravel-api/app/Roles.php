<?php

namespace App;

use illuminate\Support\Str;

use Illuminate\Database\Eloquent\Model;

class Roles extends Model
{
    protected $table = 'roles';

    protected $fillable = ['name', 'id'];

    //kita ubah Model.php panggil fungsinya disini dan manipulasi supaya uuid
    protected $keyType = 'string';
    public $incrementing = false;

    protected  static function boot()
    {
        parent::boot();

        static::creating(function ($model) {
            if (empty($model->id)) {
                $model->id = Str::uuid();
            }
        });
    }

    public function user()
    {
        return $this->belongsTo('App\User');
    }
}
