<?php

namespace App;

use illuminate\Support\Str;

use Illuminate\Database\Eloquent\Model;

class Post extends Model
{
    protected $table = 'posts';

    protected $fillable = ['title', 'description', 'user_id']; //tambahin user_id di model post dan comments untuk event, listerner dan queue

    //kita ubah Model.php panggil fungsinya disini dan manipulasi supaya uuid
    protected $keyType = 'string';
    public $incrementing = false;

    protected  static function boot()
    {
        parent::boot();

        static::creating(function ($model) {
            if (empty($model->id)) {
                $model->id = Str::uuid();
            }

            //untuk buat auth even listener user login
            $model->user_id = auth()->user()->id;
        });
    }

    public function comments()
    {
        return $this->hasMany('App\Comments');
    }
    //buat relasi nya ke tabel coments , post , user
    public function user()
    {
        return $this->belongsTo('App\User');
    }
}
