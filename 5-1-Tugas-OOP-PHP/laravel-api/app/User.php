<?php

namespace App;




use App\Roles;
use illuminate\Support\Str;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Tymon\JWTAuth\Contracts\JWTSubject;
use Illuminate\Notifications\Notifiable;
use Illuminate\Contracts\Auth\MustVerifyEmail;

class User extends Authenticatable implements JWTSubject
{
    use Notifiable; //trait

    //kenapa merah.. ingat : interface itu memiliki methot2 abstrac jadi harus dibuat dulu di bawah

    //protected $table = 'users';

    protected $fillable = ['username', 'email', 'name', 'roles_id', 'id', 'password', 'email_verified_at']; //ditambah id karna buat uuid

    //kita ubah Model.php panggil fungsinya disini dan manipulasi supaya uuid
    protected $keyType = 'string';
    public $incrementing = false;

    protected  static function boot()
    {
        parent::boot();

        // static::creating(function ($model) {// boleh pakai ini boleh pakai yang bawah
        //     if (empty($model->id)) {
        //         $model->id = Str::uuid();
        //     }
        static::creating(function ($model) {
            if (empty($model->{$model->getKeyName()})) {
                $model->{$model->getKeyName()} = Str::uuid();
            }

            $model->roles_id = Roles::where('name', 'author')->first()->id;
        });
    }


    public function roles()
    {
        return $this->hasOne('App\Roles');
    }

    public function otp_code()
    {
        return $this->hasOne('App\OtpCode');
    }



    // Rest omitted for brevity
    // jadi ada dua body untuk implement ke jwt ada function getJWTIdentifier() dan getJWTCustomClaims() liat di https://jwt-auth.readthedocs.io/en/develop/quick-start/

    /**
     * Get the identifier that will be stored in the subject claim of the JWT.
     *
     * @return mixed
     */
    public function getJWTIdentifier()
    {
        return $this->getKey();
    }

    /**
     * Return a key value array, containing any custom claims to be added to the JWT.
     *
     * @return array
     */
    public function getJWTCustomClaims()
    {
        return [];
    }

    //buat relasi nya ke tabel coments , post , user
    public function comments()
    {
        return $this->hasMany('App\Comments');
    }

    public function post()
    {
        return $this->hasMany('App\Post');
    }
}
