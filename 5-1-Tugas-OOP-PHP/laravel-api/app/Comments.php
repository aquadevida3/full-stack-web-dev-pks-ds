<?php

namespace App;

use Illuminate\Support\Str;
use Illuminate\Database\Eloquent\Model;


class Comments extends Model
{
    protected $fillable = ['content', 'post_id', 'user_id']; //tambahin user_id di model post dan comments untuk event, listerner dan queue

    protected $primaryKey = 'id';
    protected $keyType = 'string';
    public $incrementing = false;

    protected  static function boot()
    {
        parent::boot();

        static::creating(function ($model) {
            if (empty($model->id)) {
                $model->id = Str::uuid();
            }


            //untuk buat auth even listener user login
            $model->user_id = auth()->user()->id;
        });
    }

    public function post()
    {
        // return $this->hasOne('App\Post');
        return $this->belongsTo('App\Post');
    }

    //buat relasi nya ke tabel coments , post , user
    public function user()
    {
        return $this->belongsTo('App\User');
    }
}
